﻿using InterviewTest.Sql.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace InterviewTest.Sql.Data.Context
{
    public class InterviewTestDbContext : DbContext
    {
        public InterviewTestDbContext(DbContextOptions<InterviewTestDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");
            var transactionBuilder = builder.Entity<Transaction>().ToTable("Transactions");
            var fileBuilder = builder.Entity<File>().ToTable("Files");

            transactionBuilder.HasKey(x => x.Id);
            fileBuilder.HasKey(x => new { x.Id });
            fileBuilder.HasIndex(x => new { x.Id, x.Email });
        }

        internal DbSet<File> FileEntities { get; set; }

        internal DbSet<Transaction> TransactionEntities { get; set; }
    }
}

﻿using System;

namespace InterviewTest.Sql.Data.Models
{
    internal class File
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public byte[] FileData { get; set; }

        public string Email { get; set; }

        public DateTime Created { get; set; }

    }
}

﻿using System;

namespace InterviewTest.Sql.Data.Models
{
    internal class Transaction
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }
    }
}

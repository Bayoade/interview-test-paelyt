﻿using AutoMapper;
using InterviewTest.Core.Models;
using InterviewTest.Sql.Data.Models;

namespace InterviewTest.Sql.Data
{
    public class DataMapperProfile : Profile
    {
        public DataMapperProfile()
        {
            CreateMap<TransactionEntity, Transaction>()
                .ForMember(x => x.Modified, y => y.Ignore())
                .ForMember(x => x.Created, y => y.Ignore());

            CreateMap<FileEntity, File>()
                .ForMember(x => x.Created, y => y.Ignore());
        }
    }
}

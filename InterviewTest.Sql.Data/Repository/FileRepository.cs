﻿using AutoMapper;
using InterviewTest.Core.IRepository;
using InterviewTest.Core.Models;
using InterviewTest.Sql.Data.Context;
using InterviewTest.Sql.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewTest.Sql.Data.Repository
{
    public class FileRepository : IFileRepository
    {
        private readonly InterviewTestDbContext _dbContext;
        public FileRepository(InterviewTestDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task AddFileAsync(FileEntity fileEntity)
        {
            var file = Mapper.Map<File>(fileEntity);

            file.Created = DateTime.UtcNow;

            _dbContext.FileEntities.Add(file);

            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteFileAsync(params Guid[] ids)
        {
            var files = new List<File>();

            foreach (var id in ids)
            {
                files.Add(new File { Id = id });
            }

            _dbContext.FileEntities.AttachRange(files);

            _dbContext.FileEntities.RemoveRange(files);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<FileEntity> GetFileAsync(Guid id)
        {
            var file = await _dbContext.FileEntities.FirstOrDefaultAsync(x => x.Id == id);
            return Mapper.Map<FileEntity>(file);
        }

        public async Task<IList<FileEntity>> GetFilesByEmailAsync(string email)
        {
            var files = await _dbContext.FileEntities.Where(x => x.Email == email).ToListAsync();
            return Mapper.Map<List<FileEntity>>(files);
        }
    }
}

﻿using AutoMapper;
using InterviewTest.Core.IRepository;
using InterviewTest.Core.Models;
using InterviewTest.Sql.Data.Context;
using InterviewTest.Sql.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.Sql.Data.Repository
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly InterviewTestDbContext _dbContext;

        public TransactionRepository(InterviewTestDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task AddTransactionAsync(TransactionEntity transactionEntity)
        {
            var transaction = Mapper.Map<Transaction>(transactionEntity);

            transaction.Created = DateTime.UtcNow;

            _dbContext.TransactionEntities.Add(transaction);
            return _dbContext.SaveChangesAsync();
        }

        public Task DeleteTransactionAsync(params Guid[] ids)
        {
            var transactions = new List<Transaction>();

            foreach (var id in ids)
            {
                transactions.Add(new Transaction { Id = id });
            }

            _dbContext.TransactionEntities.AttachRange(transactions);

            _dbContext.TransactionEntities.RemoveRange(transactions);

            return _dbContext.SaveChangesAsync();
        }

        public async Task<TransactionEntity> GetTransactionAsync(Guid id)
        {
            var transaction = await GetTransactionByIdAsync(id);

            return Mapper.Map<TransactionEntity>(transaction);
        }

        public async Task UpdateTransactionAsync(TransactionEntity transactionEntity)
        {
            var transactionDb = await GetTransactionByIdAsync(transactionEntity.Id);

            Mapper.Map(transactionEntity, transactionDb);

            transactionDb.Modified = DateTime.UtcNow;

            _dbContext.TransactionEntities.Update(transactionDb);

            await _dbContext.SaveChangesAsync();
        }

        private Task<Transaction> GetTransactionByIdAsync(Guid id)
        {
            return  _dbContext.TransactionEntities.FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}

﻿(function () {
    'use strict';

    $(document).ready(function () {
        $('#upload-btn').on('click', handleClickUploadFiles);
    });

    var handleClickUploadFiles = function (e) {

        var email = $('#user-email').val();
        var fileUpload = $("#files").get(0);
        var files = fileUpload.files;
        // Create FormData object    
        var fileData = new FormData();
        // Looping over all files and add it to FormData object    
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        };

        $.ajax({
            url: '/File/UploadFiles?email=' + email,
            type: "POST",
            contentType: false, // Not to set any content header    
            processData: false, // Not to process data    
            data: fileData,
            success: function (result) {
                $('#file-browse').find("*").prop("disabled", true);

                LoadProgressBar(result);
            },
            error: function (err) {
                alert(err.statusText);
            }
        });
       
    };

    function LoadProgressBar(result) {
        var progressbar = $("#progressbar");
        var progressLabel = $(".progressbar-label");

        progressbar.show();
        $("#progressbar").progressbar({
            //value: false,      
            change: function () {
                progressLabel.text(
                    progressbar.progressbar("value") + "%");  // Showing the progress increment value in progress bar  
            },
            complete: function () {
                progressLabel.text("Loading Completed!");
                progressbar.progressbar("value", 0);  //Reinitialize the progress bar value 0  
                progressLabel.text("");
                progressbar.hide(); //Hiding the progress bar  
                $('#file-browse').find("*").prop("disabled", true);
            }
        });

        function progress() {
            var val = progressbar.progressbar("value") || 0;
            progressbar.progressbar("value", val + 1);
            if (val < 99) {
                setTimeout(progress, 25);
            }
        }
        setTimeout(progress, 100);
    }

})();
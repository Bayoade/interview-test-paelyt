﻿using AutoMapper;
using InterviewTest.Core.Models;
using InterviewText.Models;

namespace InterviewText
{
    public class WebMapperProfile : Profile
    {
        public WebMapperProfile()
        {
            CreateMap<TransactionViewModel, TransactionEntity>()
                .ForMember(x => x.Id, y => y.Ignore())
                .ForMember(x => x.Name, y => y.Ignore());
        }
    }
}

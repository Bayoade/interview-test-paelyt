﻿using System;

namespace InterviewText.Models
{
    public class PagedModel<T> where T : class
    {
        public Guid TransactionId { get; set; }

        public T Model { get; set; }

        public string Email { get; set; }
    }
}

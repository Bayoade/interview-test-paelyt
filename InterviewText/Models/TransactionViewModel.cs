﻿using System;

namespace InterviewText.Models
{
    public class TransactionViewModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}

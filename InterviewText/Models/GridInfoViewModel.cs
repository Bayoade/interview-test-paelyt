﻿using System;

namespace InterviewText.Models
{
    public class GridInfoViewModel
    {
        public string Email { get; set; }

        public Guid TransactionNumber { get; set; }
    }
}

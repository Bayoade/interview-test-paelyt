﻿using System;

namespace InterviewText.Models
{
    public class TransactionResultModel
    {
        public string Email { get; set; }

        public Guid TransactionNumber { get; set; }

        public string Name { get; set; }
    }
}

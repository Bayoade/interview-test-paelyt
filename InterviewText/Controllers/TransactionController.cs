﻿using AutoMapper;
using InterviewTest.Core.IService;
using InterviewTest.Core.Models;
using InterviewText.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InterviewText.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly ITransactionComposerService _transactionComposerService;
        public TransactionController(ITransactionService transactionService,
            ITransactionComposerService transactionComposerService)
        {
            _transactionService = transactionService;
            _transactionComposerService = transactionComposerService;
        }

        public IActionResult AddTransaction()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddTransaction(TransactionViewModel model)
        {
            var transaction = Mapper.Map<TransactionEntity>(model);
            var transactionId = await _transactionComposerService.TransactEmailProcess(transaction);

            return RedirectToAction("TransactionDetails", new TransactionResultModel {
                TransactionNumber = transactionId,
                Email = model.Email,
                Name = model.FirstName + "" + model.LastName });
        }

        public IActionResult TransactionDetails(TransactionResultModel model)
        {
            return View(model);
        }
    }
}
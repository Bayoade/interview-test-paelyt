﻿using InterviewTest.Core.IService;
using InterviewTest.Core.Models;
using InterviewText.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MimeMapping;

namespace InterviewText.Controllers
{
    public class FileController : Controller
    {
        private readonly IFileService _fileService;
        private readonly ITransactionService _transactionService;
        private readonly ITransactionComposerService _transactionComposerService;

        public FileController(IFileService fileService, 
            ITransactionService transactionService,
            ITransactionComposerService transactionComposerService)
        {
            _fileService = fileService;
            _transactionService = transactionService;
            _transactionComposerService = transactionComposerService;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFiles(string email)
        {
            var fileDocument = Request.Form.Files;
            var files = new List<FileEntity>();

            foreach (var formFile in fileDocument)
            {
                if (formFile.Length > 0)
                {
                    var file = new FileEntity();

                    using (var stream = formFile.OpenReadStream())
                    {
                        file.FileData = ConvertStreamToByteArray(stream);
                    }

                    file.FileName = formFile.FileName;

                    files.Add(file);
                }
            }

            await _fileService.AddFileAsync(email, files);
            return Ok(true);
        }

        public IActionResult GridDocumentTransaction()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GridDocumentTransaction(GridInfoViewModel model)
        {
            await ValidateTransaction(model);

            var fileDetails = await _transactionComposerService.GetAllFileInfoByEmail(model.Email);

            var pagedModel = new PagedModel<IList<FileDetails>>
            {
                TransactionId = model.TransactionNumber,
                Model = fileDetails
            };

            return RedirectToAction("GridDocuments", pagedModel);
        }

        public IActionResult GridDocuments(PagedModel<IList<FileDetails>> pagedModelFileDetails)
        {
            return View(pagedModelFileDetails);
        }

        public async Task<IActionResult> Delete(Guid id, string email)
        {
            await _fileService.DeleteFileAsync(id);
            return RedirectToAction("GridDocumentTransaction", new GridInfoViewModel { TransactionNumber = id, Email = email });
        }

        public async Task<IActionResult> Download(Guid id)
        {
            var file = await _fileService.GetFileAsync(id);

            var outputStream = new MemoryStream(file.FileData);
            outputStream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(outputStream, MimeUtility.GetMimeMapping(file.FileName));
        }

        private async Task ValidateTransaction(GridInfoViewModel model)
        {
            var transaction = await _transactionService.GetTransactionAsync(model.TransactionNumber);

            if (transaction.Email != model.Email)
            {
                ModelState.AddModelError("Error", "The transaction number or email is incorrect");
                throw new Exception("Invalid Model");
            };
        }

        private static byte[] ConvertStreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream memoryStream = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, read);
                }
                return memoryStream.ToArray();
            }
        }
    }
}
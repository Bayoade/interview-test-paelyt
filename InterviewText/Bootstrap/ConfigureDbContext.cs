﻿using InterviewTest.Sql.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InterviewText.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddConfigureDbContext(this IServiceCollection services)
        {
            services.AddDbContext<InterviewTestDbContext>(conf =>
            {
                conf.UseSqlServer(IoC.Configuration.GetConnectionString("InterviewDbConnectionKey"));
            });
        }
    }
}

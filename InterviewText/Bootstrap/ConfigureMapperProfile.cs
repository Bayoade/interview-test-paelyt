﻿using AutoMapper;
using InterviewTest.AppService;
using InterviewTest.Sql.Data;
using Microsoft.Extensions.DependencyInjection;

namespace InterviewText.Bootstrap
{
    public static class ConfigureMapperProfile
    {
        public static void AddMapperProfileConfiguration(this IServiceCollection services)
        {
            Mapper.Initialize(config =>
            {
                config.AddProfile<DataMapperProfile>();
                config.AddProfile<AppServiceMapperProfile>();
                config.AddProfile<WebMapperProfile>();
            });
        }
    }
}

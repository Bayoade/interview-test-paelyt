﻿using Microsoft.Extensions.Configuration;

namespace InterviewText.Bootstrap
{
    public static class IoC
    {
        public static IConfiguration Configuration { get; set; }
    }
}

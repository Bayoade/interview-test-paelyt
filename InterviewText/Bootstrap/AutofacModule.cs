﻿using Autofac;
using InterviewTest.AppService.Services;
using InterviewTest.Core.IRepository;
using InterviewTest.Core.IService;
using InterviewTest.Sql.Data.Repository;

namespace InterviewText.Bootstrap
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TransactionRepository>()
                .As<ITransactionRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TransactionService>()
                .As<ITransactionService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FileRepository>()
                .As<IFileRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterType<FileService>()
                .As<IFileService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TransactionComposerService>()
                .As<ITransactionComposerService>()
                .InstancePerLifetimeScope();
        }
    }
}

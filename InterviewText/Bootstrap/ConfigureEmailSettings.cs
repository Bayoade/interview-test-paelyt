﻿using InterviewTest.AppService.Models;
using InterviewTest.Core.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InterviewText.Bootstrap
{
    public static class ConfigureEmailSettings
    {
        public static void AddEmailConfiguration(this IServiceCollection services)
        {
            services.AddSingleton<IEmailConfiguration>(IoC.Configuration
                .GetSection("EmailConfiguration").Get<EmailConfiguration>());
        }
    }
}

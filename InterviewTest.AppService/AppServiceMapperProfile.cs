﻿using AutoMapper;
using InterviewTest.Core.Models;

namespace InterviewTest.AppService
{
    public class AppServiceMapperProfile : Profile
    {
        public AppServiceMapperProfile()
        {
            CreateMap<FileDetails, FileEntity>()
                .ForMember(x => x.FileData, y => y.Ignore());
        }
    }
}

﻿using AutoMapper;
using InterviewTest.Core.IService;
using InterviewTest.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.AppService.Services
{
    public class TransactionComposerService : ITransactionComposerService
    {
        public IServiceProvider Services { get; }

        private readonly ITransactionService _transactionService;
        private readonly IFileService _fileService;
        public TransactionComposerService(IServiceProvider services, 
            ITransactionService transactionService,
            IFileService fileService)
        {
            _transactionService = transactionService;
            Services = services;
            _fileService = fileService;
        }
        
        public async Task<Guid> TransactEmailProcess(TransactionEntity transactionEntity)
        {
            using (var scope = Services.CreateScope())
            {
                var emailProcessingService =scope.ServiceProvider
                    .GetRequiredService<IEmailService>();

                var fileProcessingService = scope.ServiceProvider
                    .GetRequiredService<IFileService>();

                var fileEntitities = await fileProcessingService.GetFilesByEmailAsync(transactionEntity.Email);
                var transactionResult = Mapper.Map<TransactionResult>(transactionEntity);
                transactionResult.Content = "This mail contains some attached files, please kindly find attached.";

                emailProcessingService.SendEmail(transactionResult, fileEntitities);
            }

            return await _transactionService.AddTransactionAsync(transactionEntity);
        }

        public async Task<IList<FileDetails>> GetAllFileInfoByEmail(string email)
        {
            var files = await _fileService.GetFilesByEmailAsync(email);

            return Mapper.Map<List<FileDetails>>(files);
        }
    }
}

﻿using InterviewTest.Core.IRepository;
using InterviewTest.Core.IService;
using InterviewTest.Core.Models;
using System;
using System.Threading.Tasks;

namespace InterviewTest.AppService.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;

        public TransactionService(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;

        }

        public async Task<Guid> AddTransactionAsync(TransactionEntity transactionEntity)
        {
            transactionEntity.Id = Guid.NewGuid();
            await _transactionRepository.AddTransactionAsync(transactionEntity);

            return transactionEntity.Id;

        }

        public Task DeleteTransactionAsync(Guid id)
        {
            return _transactionRepository.DeleteTransactionAsync(id);
        }

        public Task<TransactionEntity> GetTransactionAsync(Guid id)
        {
            return _transactionRepository.GetTransactionAsync(id);
        }

        public Task UpdateTransactionAsync(TransactionEntity transactionEntity)
        {
            return _transactionRepository.UpdateTransactionAsync(transactionEntity);
        }
    }
}

﻿using InterviewTest.Core.IRepository;
using InterviewTest.Core.IService;
using InterviewTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.AppService.Services
{
    public class FileService : IFileService
    {
        private readonly IFileRepository _fileRepository;

        public FileService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }
        public async Task AddFileAsync(string email, List<FileEntity> fileEntities)
        {
            foreach(var fileEntity in fileEntities)
            {
                fileEntity.Email = email;
                fileEntity.Id = Guid.NewGuid();

                await _fileRepository.AddFileAsync(fileEntity);
            }
        }

        public Task DeleteFileAsync(Guid id)
        {
            return _fileRepository.DeleteFileAsync(id);
        }

        public Task<FileEntity> GetFileAsync(Guid id)
        {
            return _fileRepository.GetFileAsync(id);
        }

        public Task<IList<FileEntity>> GetFilesByEmailAsync(string email)
        {
            return _fileRepository.GetFilesByEmailAsync(email);
        }
    }
}

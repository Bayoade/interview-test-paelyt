﻿using InterviewTest.Core.Models;
using System;
using System.Threading.Tasks;

namespace InterviewTest.Core.IService
{
    public interface ITransactionService
    {
        Task<Guid> AddTransactionAsync(TransactionEntity transactionEntity);

        Task<TransactionEntity> GetTransactionAsync(Guid id);

        Task DeleteTransactionAsync(Guid id);

        Task UpdateTransactionAsync(TransactionEntity transactionEntity);
    }
}

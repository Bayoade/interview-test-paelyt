﻿using InterviewTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.Core.IService
{
    public interface IFileService
    {
        Task AddFileAsync(string email, List<FileEntity> fileEntities);

        Task<FileEntity> GetFileAsync(Guid id);

        Task<IList<FileEntity>> GetFilesByEmailAsync(string email);

        Task DeleteFileAsync(Guid id);
    }
}

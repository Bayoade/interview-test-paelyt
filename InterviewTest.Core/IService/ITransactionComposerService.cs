﻿using InterviewTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.Core.IService
{
    public interface ITransactionComposerService
    {
        Task<Guid> TransactEmailProcess(TransactionEntity transactionEntity);

        Task<IList<FileDetails>> GetAllFileInfoByEmail(string email);
    }
}

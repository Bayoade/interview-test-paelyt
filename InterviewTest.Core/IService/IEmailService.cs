﻿using InterviewTest.Core.Models;
using System.Collections.Generic;

namespace InterviewTest.Core.IService
{
    public interface IEmailService
    {
        void SendEmail(TransactionResult details, IList<FileEntity> fileEntities);
    }
}

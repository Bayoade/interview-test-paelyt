﻿using System;

namespace InterviewTest.Core.Models
{
    public class FileDetails
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }
    }
}

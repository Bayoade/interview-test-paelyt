﻿using System;

namespace InterviewTest.Core.Models
{
    public class FileEntity
    {
        public Guid Id { get; set; }

        public string FileName { get; set; }

        public byte[] FileData { get; set; }

        public string Email { get; set; }
    }
}

﻿using System;

namespace InterviewTest.Core.Models
{
    public class TransactionResult
    {
        public string Email { get; set; }

        public Guid TransactionNumber { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }
    }
}

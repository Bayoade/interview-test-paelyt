﻿using InterviewTest.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InterviewTest.Core.IRepository
{
    public interface IFileRepository
    {
        Task AddFileAsync(FileEntity fileEntity);

        Task<FileEntity> GetFileAsync(Guid id);

        Task<IList<FileEntity>> GetFilesByEmailAsync(string email);

        Task DeleteFileAsync(params Guid[] ids);
    }
}

﻿using InterviewTest.Core.Models;
using System;
using System.Threading.Tasks;

namespace InterviewTest.Core.IRepository
{
    public interface ITransactionRepository
    {
        Task AddTransactionAsync(TransactionEntity transactionEntity);

        Task<TransactionEntity> GetTransactionAsync(Guid id);

        Task DeleteTransactionAsync(params Guid[] ids);

        Task UpdateTransactionAsync(TransactionEntity transactionEntity);
    }
}
